import { Card, Button } from 'react-bootstrap';
import {useState, useEffect} from 'react'; 
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {

  // console.log("Contents of props: ");
  // console.log(props);
  // console.log(typeof props);

const {name,description, price} = courseProp;

//State Hooks (useState) - a away to store information within a components and trac this information
      // getter, setter
      // variable, function to change the value of variables


  // const [count, setCount] = useState(0);// count = 0;

  // function enroll(){
  //   setCount(count + 1);

  //   console.log('Enrollees: '+ count);
  // }

//_______________________________________ACTIVITY 51__________________________________
  const [count, setSeat] = useState(0);
  const slots = 30;
  function enroll(){
    if(count < slots){
      setSeat(count + 1);
      console.log('Enrollees: ' + count);
    }
    else{
      alert("Pasensya na Ubos na ");
    }
  //  console.log('Enrollees: ' + count);
  // setSeat(slots - 1);
  // console.log('Seats: ' + count) 

  }

//   useEffect(()=>{
// if (slots === 0){
//       alert('No more seats available')
// };

//   });

//__________________________________Activity 51 _________________
  
  


  return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button variant="primary"onClick={enroll}>Enroll</Button>
              <Card.Text>Total Enrolled: {count}</Card.Text>
          </Card.Body>
      </Card>
  )
}

//Check if the CourseCard component is getting the correct property types

CourseCard.propTypes = {
  courseProp: PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.string.isRequired

  })
}