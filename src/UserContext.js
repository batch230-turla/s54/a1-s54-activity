import React from 'react';

//>> Create a Context Object
//a context objeect data type that can be use to store im\nformation that can be share to other components within the application
const UserContext =React.createContext();
//console.log("Contents of usercontents") - optional

//The "Provider" component allows other components to consume/use the context object and supply the necessary information needed in the context object

//The provider is used to create a context that can be consumed.
export const UserProvider = UserContext.Provider;

export default UserContext;