import{Navigate} from 'react-router-dom'; 
import {userEffect, useContext } from 'react';
import UserContext from '../UserContext';
export default function Logout(){

	const {unsetUser,setUser} = useContext(UserContext);
	unsetUser();

	userEffect(()=> {
		setUser({email:  null});
	})
	
	return(
		<Navigate to ="/login" />
		)
}