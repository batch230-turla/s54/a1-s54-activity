import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import{Navigate} from 'react-router-dom';


//_____________________________________ACTIVITY 52________________________

export default function Login(){


	//3. use the state
	//variable, setter function
		const{ user,setUser } = useContext(UserContext);



	//State hooks to storee the values of input fields
	const [email,setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	//state to determine whether submit button is eenable or not 
	const [isActive, setIsActive] = useState(false);

	//check if values are successfully binded
	console.log(email);
	console.log(password1);
	// 	export default function Settings(){

	// 	const {user,setUser} = useContext(UserContext);
	// 	return (
	// 		(user.email !== null)
	// 		?
	// 		<h3> Settings Page</h3>
	// 		:
	// 		<Navigate to ="/login" />

	// 		)
	// }	










	function registerUser(event){

		//Prevent page redirection via form submission
		// event.preventDefault();

		//clear input fields
		localStorage.getItem('email',email);
		setEmail('');
		setPassword1('');
		
		alert('Salamat! Isa ka nang Tunay na Rehistrado');

	}

	useEffect(()=>{
		if ((email !=='' && password1 !=='' ) && (password1 === password1)){
			setIsActive(true);
		}
		else{
		

			setIsActive(false);
		}
	},[email,password1])



	return (

		(user.email !== null)
		? // true - means email field is succesfully set	

								//_____________Activity s54_______________________	
		<Navigate to ="/courses" onClick={window.location.replace('/courses')}/>
		:
		<Form onSubmit={(event)=> registerUser(event)}>

		<h3>Login Page</h3>

		            <Form.Group controlId="userEmail">
		                <Form.Label>Email address</Form.Label>
		                <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value = {email}
			                onChange = {e => setEmail(e.target.value)}
			                required	
		                />
		                <Form.Text className="text-muted">
		                    We'll never share your email with anyone else.
		                </Form.Text>
		            </Form.Group>

		            <Form.Group controlId="password1">
		                <Form.Label>Password</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value = {password1}
			                onChange = {e => setPassword1(e.target.value)} 
			                required
		                />
		            </Form.Group>

		            {isActive ? // true //conditional render
		            	<Button variant="primary" type="submit" id="submitBtn">
		            	Submit
		            </Button>
		            : //false
		            <Button variant="secondary" type="submit" id="submitBtn " disabled>
		            	Submit
		            </Button>

		        	}
		           
		        </Form>
		)
}